import unittest
from dataclasses import dataclass, FrozenInstanceError
from typing import TypeVar, Generic
from abc import ABC, abstractmethod


class Identifier:
    def __set_name__(self, owner, name):
        owner._identifier_name = name


@dataclass(frozen=True, kw_only=True)
class Entity:
    name: str
    value: float
    identifier: str = Identifier()


@dataclass(frozen=True, kw_only=True)
class OtherEntity:
    value: int
    name: str = Identifier()


T = TypeVar("T")


class Repo(ABC, Generic[T]):
    def __init__(self):
        self._storage = dict()

    @property
    @abstractmethod
    def OBJECT_CLASS(self):
        raise NotImplementedError

    def add(self, obj: T):
        self._storage.update({
            obj.__getattribute__(self.OBJECT_CLASS._identifier_name): obj
        })

    def get_by_id(self, identifier: str) -> T:
        return self._storage.get(identifier)


class EntityRepo(Repo[Entity]):
    OBJECT_CLASS = Entity


class OtherEntityRepo(Repo[OtherEntity]):
    OBJECT_CLASS = OtherEntity


class TestEntity(unittest.TestCase):

    def test_descriptor(self):
        self.assertEqual("identifier", Entity._identifier_name)

    def test_init(self):
        e = Entity(identifier="foo", name="bar", value=10)
        self.assertEqual("identifier", e._identifier_name)
        self.assertEqual("foo", e.identifier)

    def test_still_frozen(self):
        e = Entity(identifier="foo", name="bar", value=10)
        with self.assertRaises(FrozenInstanceError):
            e.identifier = "other name"


class TestRepos(unittest.TestCase):

    def test_add_and_get_by_id_for_entity(self):
        e = Entity(identifier="foo", name="bar", value=10)
        repo = EntityRepo()
        repo.add(e)

        result = repo.get_by_id("foo")
        self.assertEqual(
            e,
            result
        )

    def test_add_and_get_by_id_for_other_entity(self):
        e = OtherEntity(value=1, name="foo")
        repo = OtherEntityRepo()
        repo.add(e)

        result = repo.get_by_id("foo")
        self.assertEqual(
            e,
            result
        )


if __name__ == '__main__':
    unittest.main()
